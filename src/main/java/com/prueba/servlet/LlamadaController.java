package com.prueba.servlet;

import java.util.List;

public class LlamadaController {
    /*public static List<Llamada> getAll()
    {
        return DBDatos.getLlamadas();
    }*/
    public static List<Llamada> getAllconEmail()
    {
        return DBDatos.getLlamadasEmail();
    }
    public static Llamada getLlamadaById(int id)
    {
        return DBDatos.getLlamadaId(id);
    }
    public static void save(Llamada llm)
    {
        if(llm.getIdLlamadas() > 0)
        {
            DBDatos.updateLlamada(llm);
        }
        else{
            DBDatos.newLlamada(llm);
        }
    }
    public static void deleteId(int id)
    {
        DBDatos.deleteLlamadaId(id);
    }
}