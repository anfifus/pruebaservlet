package com.prueba.servlet;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


import com.mysql.jdbc.Connection;



public class DBDatos {
    
    private static final String TABLE = "contactos";
    private static final String KEY = "id"; 
   
    public static Contacto newContacto(Contacto cn){
            // NO!!!! String sql =  "INSERT INTO contactos (nombre, email) VALUES ('"+cn.getNombre()+"','"+cn.getEmail()+"')";
            String sql =  "INSERT INTO contactos (nombre, email) VALUES (?,?)";
           
            try (Connection conn = DBConn.getConn();
                    PreparedStatement pstmt = conn.prepareStatement(sql);
                    Statement stmt = conn.createStatement()) {
                        
                pstmt.setString(1, cn.getNombre());
                pstmt.setString(2, cn.getEmail());
                pstmt.executeUpdate(); 
            
                //usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()"); 
                if (rs.next()) { 
                    cn.setId(rs.getInt(1));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return cn;
    }
    public static Llamada newLlamada(Llamada llm)
    {
        String sql = "INSERT INTO llamadas(notas,idcontactos,hora) VALUES(?,?,?)";
        try
        (
            Connection conn = DBConn.getConn();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            Statement stmt = conn.createStatement();
        )
        {
            pstmt.setString(1, llm.getNotas());
            pstmt.setInt(2, llm.getIdContactos());
            pstmt.setDate(3, llm.getFecha());
            pstmt.executeUpdate();
            
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
            if(rs.next())
            {
                llm.setIdLlamadas(rs.getInt(1));
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return llm;
    }


  
    public static Contacto getContactoId(int id){
        Contacto cn = null;
        String sql = String.format("select %s,nombre,email from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cn = new Contacto(
                (Integer) rs.getObject(1),
                (String) rs.getObject(2),
                (String) rs.getObject(3)
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return cn;
    }
    
    public static Llamada getLlamadaId(int id)
    {
        Llamada llm = null;
        String sql = String.format("select idllamadas,notas,idcontactos,hora from llamadas where idllamadas = %d",id);
        try 
        (
            Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement();
        ) 
        {
            ResultSet res = stmt.executeQuery(sql);
            if(res.next())
            {
                llm = new Llamada(res.getInt("idllamadas"),res.getString("notas"),res.getInt("idcontactos"),res.getDate("hora"));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return llm;
    }

    public static String getLlamadaContactoId(int id)
    {
        String nombre = null;
        String sql = String.format("SELECT nombre FROM contactos WHERE idcontactos =  %d",id);
        try 
        (
            Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement();
        ) 
        {
            ResultSet rs = stmt.executeQuery(sql);
            if(rs.next())
            {
              nombre =  rs.getString("nombre");
            }    
        }
         catch (Exception e) 
         {
            String s = e.getMessage();
            System.out.println(s);
            e.printStackTrace();
        }
        return nombre;

    }




    public static boolean updateContacto(Contacto cn){
        boolean updated = false;
        String sql = String.format("UPDATE contactos set nombre=?, email=? where id=%d", cn.getId());
    
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.executeUpdate(); 
            updated = true;
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }

    public static boolean updateLlamada(Llamada llm)
    {
        boolean updated= false;
        String sql = String.format("UPDATE llamadas SET notas = ?, idcontactos = ?, hora = ? WHERE idllamadas = %d",llm.getIdLlamadas());
        String sqlComprovar = String.format("SELECT * FROM contactos WHERE id = %d",llm.getIdContactos());
        try 
        (
            Connection conn = DBConn.getConn();
            PreparedStatement pstmt = conn.clientPrepareStatement(sql);
            Statement stmt = conn.createStatement();
        )
         {
            ResultSet rs = stmt.executeQuery(sqlComprovar);
            System.out.println(llm.getFecha());
            if(rs.next())
            {
                pstmt.setString(1, llm.getNotas());
                pstmt.setInt(2, llm.getIdContactos());
                pstmt.setDate(3, llm.getFecha());
                pstmt.executeUpdate();
                updated = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }


    public static boolean deleteContactoId(int id){
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted=true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }
    public static boolean deleteLlamadaId(int id)
    {
        boolean deleted = false;
        String sql = String.format("DELETE FROM llamadas where idllamadas = %d",id);
        try 
        (
            Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement();
        ) 
        {
            stmt.executeUpdate(sql);
            deleted = true;

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }   
        return deleted;     
    }
    


    public static List<Contacto> getContactos() {
        List<Contacto> listaContactos = new ArrayList<Contacto>();  
        String sql = "select id,nombre,email from contactos"; 
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) { 
                
                ResultSet rs = stmt.executeQuery(sql);  
                while (rs.next()) {
                    Contacto u = new Contacto(
                                (Integer) rs.getObject(1),
                                (String) rs.getObject(2),
                                (String) rs.getObject(3)); 
                    listaContactos.add(u);
                    }
        
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaContactos; 
    }
    /*public static List<Llamada> getLlamadas(){
        ArrayList<Llamada> Listallamadas = new ArrayList<Llamada>();
        String sql = "select * from llamadas";
        try 
        (
            Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement();
        )
         {
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next())
            {
                Llamada llm = new Llamada(rs.getInt("idllamadas"),rs.getString("notas"),rs.getInt("idcontactos"),rs.getDate("hora"));
                Listallamadas.add(llm);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return Listallamadas;
    }*/
    public static List<Llamada> getLlamadasEmail()
    {
        ArrayList<Llamada> Listallamadas = new ArrayList<Llamada>();
        String sql = "select idllamadas,notas,llamadas.idcontactos,hora,email,nombre from llamadas join contactos on llamadas.idcontactos = contactos.id";
        try 
        (
            Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement();
        )
         {
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next())
            {
                Llamada llm = new Llamada(rs.getInt("idllamadas"),rs.getString("notas"),rs.getInt("idcontactos"),rs.getDate("hora"),rs.getString("email"),rs.getString("nombre"));
                Listallamadas.add(llm);
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return Listallamadas;
    }


}

