package com.prueba.servlet;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;

public class Servlet extends HttpServlet{
    public static final long serialVersionUID = 12343214L;

    public void doGet(HttpServletRequest request, HttpServletResponse response )throws ServletException, IOException
    {
        /*PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title> Hola Servlet </title>");
        out.println("</head>");
        out.println("<body>");
        String name = request.getParameter("nombre");
        out.println("<h1>Adios mundo cruel </h1>"+ name);
        out.println("</body>");
        out.println("</html>");*/
        int id = Integer.parseInt(request.getParameter("id"));
        Contacto con = DBDatos.getContactoId(id);
        request.setAttribute("con", con);
        request.getRequestDispatcher("/Contacto.jsp").forward(request, response);

    }
}
