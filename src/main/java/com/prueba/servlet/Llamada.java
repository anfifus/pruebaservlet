package com.prueba.servlet;

import java.nio.file.FileSystemAlreadyExistsException;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class Llamada{
    private int idLlamadas;
    private String notas;
    private int idContactos;
    private Date fecha;
    private String email;
    private String nombre;

    public Llamada(int idLlamadas,String notas, int idContactos, Date fecha,String email)
    {
        this.fecha = fecha;
        this.idContactos = idContactos;
        this.idLlamadas = idLlamadas;
        this.notas = notas;
        this.email = email;
    }
    public Llamada(int idLlamadas,String notas, int idContactos, Date fecha,String email, String nombre)
    {
        this.fecha = fecha;
        this.idContactos = idContactos;
        this.idLlamadas = idLlamadas;
        this.notas = notas;
        this.email = email;
        this.nombre = nombre;
    }
    public Llamada(int idLlamadas,String notas, int idContactos, Date fecha)
    {
        this.fecha = fecha;
        this.idContactos = idContactos;
        this.idLlamadas = idLlamadas;
        this.notas = notas;
    }
    public Llamada(String notas,int idContactos, String fecha)
    {
        java.util.Date fechaD = null;
        try
        {
            SimpleDateFormat formdate = new SimpleDateFormat("yyyy-MM-dd");
            fechaD =  formdate.parse(fecha);
            long seg = fechaD.getTime();
            this.fecha = new Date(seg);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        this.notas = notas;
        this.idContactos = idContactos;
        
    }
    public Llamada(int idLlamadas,String notas, int idContactos, String fecha, String email,String nombre)
    {
        
        java.util.Date fechaD = null;
        try
        {
            SimpleDateFormat formdate = new SimpleDateFormat("yyyy-MM-dd");
            fechaD =  formdate.parse(fecha);
            long seg = fechaD.getTime();
            this.fecha = new Date(seg);
            System.out.println(fecha);
            System.out.println(this.fecha);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        
        this.idContactos = idContactos;
        this.idLlamadas = idLlamadas;
        this.notas = notas;
        this.email = email;
        this.nombre = nombre;
    }
    public Llamada(int idLlamadas,String notas, int idContactos, String fecha)
    {
        
        java.util.Date fechaD = null;
        try
        {
            SimpleDateFormat formdate = new SimpleDateFormat("yyyy-MM-dd");
            fechaD =  formdate.parse(fecha);
            long seg = fechaD.getTime();
            this.fecha = new Date(seg);
            System.out.println(fecha);
            System.out.println(this.fecha);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        
        this.idContactos = idContactos;
        this.idLlamadas = idLlamadas;
        this.notas = notas;
    }
    @Override
    public String toString() {
        return String.format("%d %d %d %s",idLlamadas,notas,idContactos, fecha.toString());
    }

    public int getIdLlamadas() {
        return idLlamadas;
    }

    public void setIdLlamadas(int idLlamadas) {
        this.idLlamadas = idLlamadas;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int getIdContactos() {
        return idContactos;
    }

    public void setIdContactos(int idContactos) {
        this.idContactos = idContactos;
    }

    public Date getFecha() {
        return fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
}